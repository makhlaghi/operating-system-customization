Install TeX live and all necessary packages
-------------------------------------------

1. Download the tarball, unpack it and run tlmgr:
     $ wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
     $ tar xf install-tl-unx.tar.gz
     $ cd install-tl-XXXXXXX
     $ su
     # ./install-tl

2. Scheme: "basic scheme (plain and latex)"

3. Install the packages suggested in the Gnuastro manual (under
   bootstrap dependencies):
   https://www.gnu.org/software/gnuastro/manual/html_node/Bootstrapping-dependencies.html

4. As of April 2024, in Arch, it is necessary to install
   'libxcrypt-compat' otherwise Biber will complain about not finding
   'libcrypt.so.1' and abort:

   # pacman -S libxcrypt-compat

5. These are the packages I have needed for my own work so far.

   # tlmgr install extsizes pstricks pst-plot multido pst-tools \
                   pstricks-add pst-node pst-3d pst-math pst-tree \
                   pst-grad pst-coil pst-text pst-eps pst-fill ulem \
                   footmisc datetime fmtcount setspace eso-pic \
                   titlesec preprint courier symbol zapfchan helvetic \
                   enumitem lastpage ec newtx txfonts index xetex \
                   beamer tcolorbox environ trimspaces l3kernel \
                   l3packages fp lm pst-arrow translator fontaxes \
                   tex-gyre boondox newlfm fancyhdr fancybox pdfcrop \
		   sectsty pgfgantt fontspec luaotfload eurosym

6. Generally, you can always add new packages with a similar 'tlmgr
   install' command (tlmgr is a TeX-only package manager). If you want
   to see what package a certain file is from (to fix an error in
   LaTeX for example), you can use 'tlmgr info XXXX', where 'XXXX' is
   the name of the file. 'tlmgr' will then give you a list





Increase memory if needed
-------------------------

See the PGFplots manual.

Generally, if LaTeX is unable to make one of the figures due to
memory, then follow the instructions in Chapter 6 of the PGFPlots
manual (a few short steps) to increase the default memory. In short,
put the following lines in the top file of your TeXLive build
tree. First make this file (if TeX is installed system-wide, you have
to be root, also the year might have changed, don't type the $ or #
signs):

   $ su
   # mkdir /usr/local/texlive/YEAR/myconf
   # emacs /usr/local/texlive/YEAR/myconf/texmf.cnf

Then put these lines in it (you can remove the initial empty spaces):

main_memory = 12000000       % Words of inimemory available.
extra_mem_top = 10000000     % Extra high memory for chars.
extra_mem_bot = 10000000     % Extra low memory for boxes.
save_size  = 150000          % For saving values outside current group.
stack_size = 150000          % Simultaneous input sources.

Save the file and exit, then run texhash:

   # texhash
   # exit

Then put the following line in your .bashrc file (the command `$ gedit
~/.bashrc');

     export TEXMFCNF=/usr/local/texlive/YEAR/myconf:

with a log-out and log back in things should be working properly.





Interesting packages
--------------------

These are interesting packages that might be worth looking into.

  - Bundledoc
