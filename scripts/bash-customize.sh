# Useful functions to load into the system's .bashrc.
#
# Copyright (C) 2020-2022 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script. If not, see <http://www.gnu.org/licenses/>.





# Shell history:
#   - HISTSIZE: maximum number of lines in active shell history
#     (number of lines printed by the 'history' command).
#   - HISTFILESIZE: maximum number of lines in '.bash_history' (that
#     is loaded into the active shell history on the start of every
#     shell (for infinity, set this to '-1').
#   - HISTFILE: Name of the shell history file (by deafult:
#     '.bash_history').
HISTSIZE=1000
HISTFILESIZE=20000





# Better readability of basic commands.
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'





# Add the Git branch information to the command prompt only when Git
# is present. Also set the command-prompt color to purple for normal
# users and red when the root is running it.
if git --version &> /dev/null; then
    parse_git_branch() {
	git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
    }
else
    parse_git_branch() { echo &> /dev/null; }
fi
if [ x$(whoami) = xroot ]; then
    export PS1="[\[\033[01;31m\]\u@\h \W\[\033[32m\]\$(parse_git_branch)\[\033[00m\]]# "
else
    export PS1="[\[\033[01;35m\]\u@\h \W\[\033[32m\]\$(parse_git_branch)\[\033[00m\]]$ "
fi





# A simple function to generate random strings useful for passwords.
good-password-generate() {
    cat /dev/urandom \
	| tr -dc 'a-zA-Z0-9-_!@#$%^&*()_+{}|:<>?=' \
	| fold -w 15 \
	| grep -i '[!@#$%^&*()_+{}|:<>?=]' \
	| head -n 1
}





# Extract pages from a PDF.
pdf-extract-pages() {

    # Report if there aren't enough arguments.
    if [ "x$1" = x ] || [ "x$2" = x ] || [ "x$3" = x ] || [ "x$4" = x ]; then
	echo "${FUNCNAME[0]}: 4 arguments needed: "
	echo " 1) Input PDF name. 2) Start page. 3) End page. 4) Output name."
	return 1
    fi

    # Use Ghostscript to extract the given pages.
    gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER \
       -dFirstPage=$2 \
       -dLastPage=$3 \
       -sOutputFile="$4" \
       "$1"
}





# Scale the second input to the width of the first, then combine them
# with the name of the third input.
pdf-scale-combine() {

    # Report if there aren't enough arguments.
    if [ "x$1" = x ] || [ "x$2" = x ] || [ "x$3" = x ]; then
	echo "${FUNCNAME[0]}: 3 arguments needed: "
	echo " 1) Reference PDF name 2) PDF to scale 3) Output name."
	return 1
    fi

    # Get the width of the reference image and calculate the new
    # height based on it.
    newwidth=$(pdfinfo $1 | awk '/^Page size/{print $3}')
    newheight=$(pdfinfo $2 | awk -vrw=$newwidth '/^Page size/{print $5*(rw/$3)}')

    # Scale the second PDF to be the same width as the first.  For
    # some reason, we need the '0's added to the new height and width
    # so it becomes the proper value.
    scaled=${1%.pdf}-scaled.pdf
    echo "Scaling '$2' to be $newwidth x $newheight"
    gs -q -sOutputFile=$scaled -sDEVICE=pdfwrite \
       -dCompatibilityLevel=1.4 \
       -dNOPAUSE -dBATCH \
       -dDEVICEWIDTH=$newwidth"0" \
       -dDEVICEHEIGHT=$newheight"0" \
       -dPDFFitPage $2;

    # Add the first to the scaled image.
    echo "Combining the two files..."
    gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite \
       -sOutputFile=$3 $1 $scaled
    echo "Combined PDF file: $3"

    # Clean up.
    rm $scaled
}





# Set the PDF to 300dpi. This will usually greatly reduce the size of
# the scanned PDFs.
pdf-to-300dpi() {

    # Report if there aren't enough arguments.
    if [ "x$1" = x ] || [ "x$2" = x ]; then
	echo "${FUNCNAME[0]}: 2 arguments needed: "
	echo " 1) Input PDF name 2) Output PDF name"
	return 1
    fi

    # The '\prepress' corresponds to 300dpi, but other options are
    # also available:
    #    \screen (72 dpi)
    #    \ebook (150 dpi)
    #    \printer (300 dpi)
    #    \prepress (300 dpi, but larger file)
    #    \default (output useful in many scenarios, large file!)
    gs -sDEVICE=pdfwrite \
       -dCompatibilityLevel=1.4 \
       -dPDFSETTINGS=/printer \
       -dNOPAUSE \
       -dQUIET \
       -dBATCH \
       -sOutputFile="$2" \
       "$1"
}
