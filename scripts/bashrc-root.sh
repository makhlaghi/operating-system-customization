# Useful functions to load into the system's .bashrc.
#
# Copyright (C) 2020-2022 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script. If not, see <http://www.gnu.org/licenses/>.



# Shell customizations
#
# Note that unlike variables, functions and aliases are not passed to
# sub-processes (sub-shells), so we'll have to ask the shell to define
# them every time (even if they are loaded by bash_profile).
source /home/@USERID@/.bash_custom_dir/bash-customize.sh
