# System-wide startup
# -------------------
#
# This file will be loaded by the root operating system upon boot
# (long before '.bash_profile' or '.bashrc'). It should thus be used
# for basic settings that you want for all users including the
# root. The name of this file starts with `zzz' so it will be the last
# file read in the `/etc/profile.d' directory.
#
# Copyright (C) 2020-2022 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script. If not, see <http://www.gnu.org/licenses/>.





# Set the PATHs. NOTE the colon (`:') in the end of the
# INFOPATH. Without a column finishing INFOPATH, Info will not look
# into default system-wide directories. If INFOPATH is blank before
# this command, this can be vital for accessing other installed Info
# files.
export PATH=$PATH:/usr/local/texlive/@TEXLIVEYEAR@/bin/x86_64-linux
export MANPATH=$MANPATH:/usr/local/texlive/@TEXLIVEYEAR@/texmf-dist/doc/man
export INFOPATH=$INFOPATH:/usr/local/texlive/@TEXLIVEYEAR@/texmf-dist/doc/info:





# Dynamic library search path to include programs built manually:
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
